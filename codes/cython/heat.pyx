cimport numpy as c_np
cimport cython
from cython.parallel import prange
import numpy as np

@cython.boundscheck(False)
@cython.wraparound(False)
def K2D_para(c_np.ndarray[double, ndim=2] X):

    cdef Py_ssize_t m = X.shape[0]
    cdef Py_ssize_t n = X.shape[1]
    cdef double[:,:] H_view
    cdef int i, j
    H = np.zeros((m,n))
    H_view = H
    for i in prange(1,m-1,nogil=True):
        for j in range(1,n-1):
            H_view[i,j] = 4. * X[i,j] - X[i-1, j] -X[i+1,j] - X[i, j-1] -X[i, j+1]
    return H

@cython.boundscheck(False)
@cython.wraparound(False)
def K2D_seq(c_np.ndarray[double, ndim=2] X):

    cdef Py_ssize_t m = X.shape[0]
    cdef Py_ssize_t n = X.shape[1]
    cdef double[:,:] H_view
    cdef int i, j
    H = np.zeros((m,n))
    H_view = H
    for i in range(1,m-1):
        for j in range(1,n-1):
            H_view[i,j] = 4. * X[i,j] - X[i-1, j] -X[i+1,j] - X[i, j-1] -X[i, j+1]
    return H
