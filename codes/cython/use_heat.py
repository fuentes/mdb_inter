#!/usr/bin/python
from heat import K2D_seq, K2D_para
from timeit import timeit
import sys
import os
from numpy import zeros, float64, sqrt, linspace
from numpy.linalg import norm
sys.path.append(os.getcwd()+"/build")


def K2D_array_slow(X):
    H = zeros(X.shape, dtype=float64)
    H[1:-1, 1:-1] = 2. * X[1:-1, 1:-1] - X[0:-2, 1:-1] - X[2:, 1:-1] +\
                   2. * X[1:-1, 1:-1] - X[1:-1, 0:-2] - X[1:-1, 2:]
    return H


def K2D_loop_slow(X):
    m, n = X.shape
    H = zeros((m, n), dtype=float64)
    for i in range(1, m-1):
        for j in range(1, n-1):
            H[i, j] = 4. * X[i, j] - X[i-1,  j] - \
                      X[i+1, j] - X[i, j-1] - X[i, j+1]
    return H


def setup():
    N = 1000 
    u = linspace(0, 1, N)
    X_in = sqrt(u[None, :]) * u[:, None]**3
    X_in[0, :] = 0
    X_in[-1, :] = 0
    X_in[:, 0] = 0
    X_in[:, -1] = 0
    return X_in


X_in = setup()
res_cython = K2D_seq(X_in)
res_loop_slow = K2D_loop_slow(X_in)
res_array_slow = K2D_array_slow(X_in)
res_cython_para = K2D_para(X_in)
assert norm(res_array_slow-res_loop_slow) < 1e-12
assert norm(res_loop_slow-res_cython) < 1e-12
assert norm(res_loop_slow-res_cython_para) < 1e-12


setup_str = """
from heat import K2D_seq, K2D_para
from numpy import linspace,sqrt, zeros, float64
def K2D_array_slow(X):
    H = zeros(X.shape, dtype=float64)
    H[1:-1, 1:-1] = 2. * X[1:-1, 1:-1] - X[0:-2, 1:-1] - X[2:, 1:-1] + \
                    2. * X[1:-1, 1:-1] - X[1:-1, 0:-2] - X[1:-1, 2:]
    return H


def K2D_loop_slow(X):
    m, n = X.shape
    H = zeros((m, n), dtype=float64)
    for i in range(1, m-1):
        for j in range(1, n-1):
            H[i, j] = 4. * X[i, j] - X[i-1,  j] - \
                      X[i+1, j] - X[i, j-1] - X[i, j+1]
    return H


def setup():
    N = 100
    u = linspace(0, 1, N)
    X_in = sqrt(u[None, :]) * u[:, None]**3
    X_in[0, :] = 0
    X_in[-1, :] = 0
    X_in[:, 0] = 0
    X_in[:, -1] = 0
    return X_in
X_in = setup()
"""
print("| algorithm  |  time(s)   |")
print("|------------|--------- --|")
print("| Cython //  | %.3es |"
      % (timeit("K2D_para(X_in)", setup=setup_str, number=100)/100))
print("| Numpy      | %.3es |"
      % (timeit("K2D_array_slow(X_in)", setup=setup_str, number=100)/100))
print("| Cython     | %.3es |"
      % (timeit("K2D_seq(X_in)", setup=setup_str, number=100)/100))
print("| loop       | %.3es |"
      % (timeit("K2D_loop_slow(X_in)", setup=setup_str, number=100)/100))
