#include <stdio.h>
#include <math.h>

int sum_int(const int n) {
    double accu  = 1;
    for(int i = 2; i < n; i++)
       accu += i*i;
    return accu;
}

double sum_exp(const int n) {
    double accu  = 0;
    for(int i = 0; i < n; i++)
       accu += exp(sqrt(i*1./n));
    return accu;
}

double norm_vect(const int n, const double * x)
{
    double accu  = 0;
    for(int i = 0; i < n; i++)
       accu += x[i]*x[i];
    return sqrt(accu);
}
