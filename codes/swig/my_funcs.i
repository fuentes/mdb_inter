/* File: example.i */
%module my_funcs
%{
   #define SWIG_FILE_WITH_INIT
   #include "my_funcs.h"
%}
%include "numpy.i"
%init %{
import_array();
%}

%apply( int DIM1,  double * IN_ARRAY1 ) {(const int n, const double * x) };
%include "my_funcs.h"
