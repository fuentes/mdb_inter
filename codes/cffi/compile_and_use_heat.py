#!/usr/bin/python
from numpy import zeros, float64, sqrt, linspace
from numpy.linalg import norm

from cffi import FFI
from os.path import exists
from os import system
from numpy import arange
# test the lib exists
if not exists("heat.so"):
    print("the shared lib does not not exists, we create it")
    system("gcc -O3 -shared -o heat.so heat.c")  # FIXME : check the return value
# parametrize the ffi interface
ffi = FFI()
ffi.cdef("void heat (int m, int n, const double *u_in, double *u_out);")
ffi_h = ffi.dlopen("./heat.so")

N = 3000
u = linspace(0, 1, N)
X_in = sqrt(u[None, :]) * u[:, None]**3
X_in[0, :] = 0
X_in[-1, :] = 0
X_in[:, 0] = 0
X_in[:, -1] = 0
X_out = zeros(X_in.shape)
err = 0
p_in = ffi.cast("double *", X_in.ctypes.data)
p_out = ffi.cast("double *", X_out.ctypes.data)
ffi_h.heat(N, N, p_in, p_out)
