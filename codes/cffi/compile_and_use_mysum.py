#!/usr/bin/python
from cffi import FFI
from os.path import exists
from os import system
from numpy import arange
# test the lib exists
if not exists("mysum.so"):
    print("the shared lib does not not exists, we create it")
    system("gcc -shared -o mysum.so mysum.c")  # FIXME : check the return value

# parametrize the ffi interface
ffi = FFI()
ffi.cdef("double mysum(int m, int n, double * x);")
ffi_h = ffi.dlopen("./mysum.so")

# retrieve C data without copy
hilb = 1. / (arange(1., 5.)[:, None] + arange(1., 5.)[None, :] - 1.)
p_hilb = ffi.cast("double *", hilb.ctypes.data) # 

# p_hilb = ffi.cast("double *", ffi.from_buffer(hilb))
assert abs(ffi_h.mysum(*hilb.shape, p_hilb) - 533/105.) < 1e-12
