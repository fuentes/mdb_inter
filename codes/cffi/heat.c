void heat (int m, int n, const double *u_in, double *u_out)
{
  int i, j;
  for (i = 1; i < m - 1; ++i)
  {
      for (j = 1; j < n - 1; ++j)
      {
          u_out[i * m + j] = 4 * u_in[ i * m + j]  - u_in[(i - 1) * m + j] - u_in[i * m + j + 1]  
                                                   - u_in[(i + 1) * m + j] - u_in[i * m + j - 1]; 
      }
  }
}
