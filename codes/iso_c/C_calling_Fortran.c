#include <stdio.h>
#include <stdlib.h>
#include "mat_utils.h"

extern void heat_fortran(int m, int n, double * u_in, double * u_out);


int main()
{
  int m, n, i, j ;
  double * X_in, *X_out;
  m = n = 5;
  X_in  = (double *) calloc(m*n, sizeof(double));
  X_out  = (double *) calloc(m*n, sizeof(double));

  for(int i = 0; i < m; ++i) X_in[ i * n + 0 ] = 1.;
  for(int i = 0; i < m; ++i) X_in[ i * n + (n-1) ] = 1.;
  for(int j = 0; j < n; ++j) X_in[ 0 * n + j ] = 1.;
  for(int j = 0; j < n; ++j) X_in[ (m-1) * n + j ] = 1.;

  print_mat(m, n, X_in);

  heat_fortran(m, n, X_in, X_out);

  print_mat(m, n, X_out);
 

}
