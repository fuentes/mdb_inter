program call_C
   use iso_c_binding
   implicit none
   
   interface 
     subroutine heat_c(m, n, X_in, X_out) bind(c, name='heat_c')
         use, intrinsic :: iso_c_binding
         integer(c_int), value :: m,n
         real(c_double)  :: X_in(*), X_out(*)
     end subroutine heat_c
   end interface

   integer(c_int) :: m,n
   real(c_double), allocatable, dimension(:,:) :: X_in, X_out

   m = 5 
   n = 5
   allocate(X_in(m, n), X_out(m, n))

   ! initialize X_init
   X_in(1, :) = 1.d0
   X_in(m, :) = 1.d0
   X_in(:, 1) = 1.d0
   X_in(:, n) = 1.d0

   call print_mat(X_in)

   call heat_c(m, n, X_in, X_out)

   call print_mat(X_out)

contains 
    subroutine print_mat(X)

       implicit none
       real(c_double), dimension(:,:), intent(in) :: X
       character(len=32) :: mat_format 
       write (mat_format, '(ai0ai0a)') "(", size(X, 1), "(", size(X, 2), "f9.2/))"
       print mat_format, X

    end subroutine print_mat


end program call_C
