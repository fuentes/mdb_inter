#include "heat.h"

void heat_c( int m, int n, const double *u_in, double *u_out)
{

    int i, j;

    for (j = 1; j < n - 1; ++j)
    {
        for (i = 1; i < m - 1; ++i)
        {
            u_out[i * n + j] = 4. * u_in[ i * n + j]  - u_in[(i - 1) * n + j] - u_in[i * n + j + 1]
                                                      - u_in[(i + 1) * n + j] - u_in[i * n + j - 1];  
        }
    }
}
