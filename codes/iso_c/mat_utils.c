#include <stdio.h>
#include "mat_utils.h"

void
print_mat (int size_x, int size_y, const double *u)
{

  int i, j;
  printf ("\n[ ");
  for (j = 0; j < size_y; ++j)
	{
	  printf ("%f, ", u[0 * size_y + j]);
	}
  printf("\r\n");
  for (i = 1; i < size_x - 1; ++i)
    {
      printf("  ");
      for (j = 0; j < size_y - 1; ++j)
	{
	  printf ("%f, ", u[i * size_y + j]);
	}
      printf ("%f\n", u[i * size_y + size_y - 1]);
    }
  printf("  ");
  for (j = 0; j < size_y - 1; ++j)
    {
        printf ("%f, ", u[(size_x - 1) * size_y + j]);
    }
  printf ("%f ]\n", u[(size_x - 1) * size_y + size_y - 1]);
}
