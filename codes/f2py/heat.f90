subroutine kernel(u_in, u_out, m, n ) 

    implicit none
    integer, intent(in) :: m, n
    real(8), dimension( 1:m, 1:n ), intent(in) :: u_in
    real(8), dimension( 1:m, 1:n ), intent(out) :: u_out

    u_out(2:m-1,2:n-1) = 4.d0 * u_in(2:m-1, 2:n-1) &
                              - u_in(1:m-2, 2:n-1) &
                              - u_in(3:m, 2:n-1)   &
                              - u_in(2:m-1,1:n-2)  &
                              - u_in(2:m-1,3:n)

end subroutine kernel

subroutine kernel_loop(u_in,  u_out,  m, n ) 

    implicit none
    integer, intent(in) :: m, n
    real(8), dimension( 1:m, 1:n ), intent(in) :: u_in
    real(8), dimension( 1:m, 1:n ), intent(out) :: u_out
    integer :: i,j

    do concurrent(j=2:n-1)
       do concurrent(i=2:m-1)
    u_out(i, j) = 4.d0 * u_in(i, j) - u_in(i-1, j) - u_in(i, j-1) &
                                       - u_in(i+1, j) - u_in(i, j+1)
       end do
    end do

end subroutine kernel_loop

