#!/usr/bin/python

from timeit import timeit
setup_str = """
from heat import kernel, kernel_loop
from numpy import linspace, sqrt, zeros, float64

def kernel_numpy(X):
    H = zeros(X.shape, dtype=float64)
    H[1:-1, 1:-1] = 2. * X[1:-1, 1:-1] - X[0:-2, 1:-1] - X[2:, 1:-1] + \
                    2. * X[1:-1, 1:-1] - X[1:-1, 0:-2] - X[1:-1, 2:]
    return H
N = 1000
u = linspace(0, 1, N)
X_in = sqrt(u[None, :]) * u[:, None]**3
X_in[0, :] = 0
X_in[-1, :] = 0
X_in[:, 0] = 0
X_in[:, -1] = 0
"""

print("kernel %.3es"
              % (timeit("kernel(X_in)", setup=setup_str, number=100)/100))
print("kernel_loop %.3es"
              % (timeit("kernel_loop(X_in)", setup=setup_str, number=100)/100))
print("kernel_numpy %.3es"
              % (timeit("kernel_numpy(X_in)", setup=setup_str, number=100)/100))
